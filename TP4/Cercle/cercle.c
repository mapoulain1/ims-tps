#include "cercle.h"
#include <stdio.h>

void effacerCercle(ObjetGraphiqueThis this) {
	Cercle *cercle = (struct Cercle *)this;
	cercle->super.class->setX(0, this);
	cercle->super.class->setY(0, this);
	cercle->class->setRayon(0, cercle);
}

void afficherCercle(ObjetGraphiqueThis this) {
	Cercle *cercle = (struct Cercle *)this;
	printf("<Cercle> {r: %d x:%d y:%d}\n", cercle->class->getRayon(cercle), cercle->super.class->getX(this), cercle->super.class->getY(this));
}

void deplacerCercle(ObjetGraphiqueThis this, int in_dx, int in_dy) {
	Cercle *cercle = (struct Cercle *)this;
	cercle->super.class->setX(cercle->super.class->getX(this) + in_dx, this);
	cercle->super.class->setY(cercle->super.class->getY(this) + in_dy, this);
}

int getCentreXCercle(ObjetGraphiqueThis this) {
	Cercle *cercle = (struct Cercle *)this;
	return cercle->super.class->getX(this);
}

int getCentreYCercle(ObjetGraphiqueThis this) {
	Cercle *cercle = (struct Cercle *)this;
	return cercle->super.class->getY(this);
}
