#include "metaCercle.h"

extern MetaObjetGraphique ObjetGraphiqueClass;

MetaCercle CercleClass = {&ObjetGraphiqueClass, setRayon, getRayon, ConstructeurCercle};

void setRayon(int in_rayon, CercleThis this) {
	this->rayon = in_rayon;
}

int getRayon(CercleThis this) {
	return this->rayon;
}

void ConstructeurCercle(CercleThis this) {
	ObjetGraphiqueClass.ConstructeurObjetGraphique(&(this->super));
	this->super.type = CERCLE;
	this->class = &CercleClass;
	this->rayon = 0;
}
