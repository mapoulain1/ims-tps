#include "metaObjetGraphique.h"
#include "../Cercle/cercle.h"
#include "../Rectangle/rectangle.h"
#include <stdio.h>

MetaObjetGraphique ObjetGraphiqueClass =
	{
		setX,
		setY,
		getX,
		getY,
		{effacerRectangle, effacerCercle},
		{afficherRectangle, afficherCercle},
		{deplacerRectangle, deplacerCercle},
		{getCentreXRectangle, getCentreXCercle},
		{getCentreYRectangle, getCentreYCercle},
		effacer,
		afficher,
		deplacer,
		getCentreX,
		getCentreY,
		0,
		GetNbObjetGraphique,
		ConstructeurObjetGraphique};

void setX(int in_x, ObjetGraphiqueThis this) {
	this->x = in_x;
}

void setY(int in_y, ObjetGraphiqueThis this) {
	this->y = in_y;
}

int getX(ObjetGraphiqueThis this) {
	return this->x;
}

int getY(ObjetGraphiqueThis this) {
	return this->y;
}

void effacer(ObjetGraphiqueThis this) {
	ObjetGraphiqueClass.TVMeffacer[this->type](this);
}

void afficher(ObjetGraphiqueThis this) {
	ObjetGraphiqueClass.TVMafficher[this->type](this);
}

void deplacer(ObjetGraphiqueThis this, int in_dx, int in_dy) {
	ObjetGraphiqueClass.TVMdeplacer[this->type](this, in_dx, in_dy);
}

int getCentreX(ObjetGraphiqueThis this) {
	return ObjetGraphiqueClass.TVMgetCentreX[this->type](this);
}

int getCentreY(ObjetGraphiqueThis this) {
	return ObjetGraphiqueClass.TVMgetCentreY[this->type](this);
}

int GetNbObjetGraphique(void) {
	return ObjetGraphiqueClass.NbObjetGraphique;
}

void ConstructeurObjetGraphique(ObjetGraphiqueThis this) {
	ObjetGraphiqueClass.NbObjetGraphique++;
	this->class = &ObjetGraphiqueClass;
	this->x = 0;
	this->y = 0;
}
