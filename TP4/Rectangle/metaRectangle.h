#pragma once
#include "../ObjetGraphique/metaObjetGraphique.h"
#include "rectangle.h"

typedef struct Rectangle *RectangleThis;
typedef struct MetaRectangle MetaRectangle;

struct MetaRectangle {
	struct MetaObjetGraphique *superMetaClasse;
	void (*setHauteur)(int, RectangleThis);
	void (*setLargeur)(int, RectangleThis);
	int (*getHauteur)(RectangleThis);
	int (*getLargeur)(RectangleThis);
	void (*ConstructeurRectangle)(RectangleThis);
};

void setHauteur(int, RectangleThis);
int getHauteur(RectangleThis);
void setLargeur(int, RectangleThis);
int getLargeur(RectangleThis);
void ConstructeurRectangle(RectangleThis);
