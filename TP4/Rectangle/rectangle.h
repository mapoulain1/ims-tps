#pragma once
#include "../ObjetGraphique/metaObjetGraphique.h"
#include "metaRectangle.h"

typedef struct Rectangle Rectangle;

struct Rectangle {
	struct ObjetGraphique super;
	struct MetaRectangle *class;
	int hauteur;
	int largeur;
};

void effacerRectangle(ObjetGraphiqueThis this);
void afficherRectangle(ObjetGraphiqueThis this);
void deplacerRectangle(ObjetGraphiqueThis this, int in_dx, int in_dy);
int getCentreXRectangle(ObjetGraphiqueThis this);
int getCentreYRectangle(ObjetGraphiqueThis this);