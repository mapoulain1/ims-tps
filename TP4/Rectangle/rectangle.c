#include "rectangle.h"
#include <stdio.h>

void effacerRectangle(ObjetGraphiqueThis this) {
	Rectangle *rectangle = (struct Rectangle *)this;

	rectangle->super.class->setX(0, this);
	rectangle->super.class->setY(0, this);
	rectangle->class->setHauteur(0, rectangle);
	rectangle->class->setLargeur(0, rectangle);
}

void afficherRectangle(ObjetGraphiqueThis this) {
	Rectangle *rectangle = (struct Rectangle *)this;
	printf("<Rectangle> {w:%d h:%d x:%d y:%d}\n", rectangle->class->getLargeur(rectangle), rectangle->class->getHauteur(rectangle), rectangle->super.class->getX(this), rectangle->super.class->getY(this));
}

void deplacerRectangle(ObjetGraphiqueThis this, int in_dx, int in_dy) {
	Rectangle *rectangle = (struct Rectangle *)this;
	rectangle->super.class->setX(rectangle->super.class->getX(this) + in_dx, this);
	rectangle->super.class->setY(rectangle->super.class->getY(this) + in_dy, this);
}

int getCentreXRectangle(ObjetGraphiqueThis this) {
	Rectangle *rectangle = (struct Rectangle *)this;
	return rectangle->super.class->getX(this) + rectangle->class->getLargeur(rectangle) / 2;
}

int getCentreYRectangle(ObjetGraphiqueThis this) {
	Rectangle *rectangle = (struct Rectangle *)this;
	return rectangle->super.class->getY(this) + rectangle->class->getHauteur(rectangle) / 2;
}
