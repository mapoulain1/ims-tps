#pragma once

#define TAILLE_MAX 5
typedef enum { FAUX = 0,
			   VRAI = 1 } bool;

#define DECLARER_PILE(TYPE)                                   \
	typedef struct Pile##TYPE {                               \
		TYPE pile[TAILLE_MAX];                                \
		TYPE *top;                                            \
        MetaPile##TYPE *class;                               \
	} Pile##TYPE##_t;                                         \
	void empiler##TYPE(TYPE valeur, struct Pile##TYPE *this); \
	TYPE depiler##TYPE(struct Pile##TYPE *this);              \
	bool estVide##TYPE(struct Pile##TYPE *this);              \
	TYPE sommet##TYPE(struct Pile##TYPE *this);               \

#define IMPLEMENTER_PILE(TYPE)                                 \
	void empiler##TYPE(TYPE valeur, struct Pile##TYPE *this) { \
		*(this->top) = valeur;                                 \
		(this->top)++;                                         \
	}                                                          \
                                                               \
	TYPE depiler##TYPE(struct Pile##TYPE *this) {              \
		(this->top)--;                                         \
		return *(this->top);                                   \
	}                                                          \
	bool estVide##TYPE(struct Pile##TYPE *this) {              \
		return this->top == NULL || this->top == this->pile;   \
	}                                                          \
	TYPE sommet##TYPE(struct Pile##TYPE *this) {               \
		return *(this->top - 1);                               \
	}

#define DECLARER_META_PILE(TYPE)                               \
	typedef struct Pile##TYPE *Pile##TYPE##This;               \
	typedef struct MetaPile##TYPE {                            \
		void (*construire)(Pile##TYPE##This);                  \
        void (*empiler)(TYPE valeur, Pile##TYPE##This);        \
        TYPE (*depiler)(Pile##TYPE##This);                     \
        TYPE (*sommet)(Pile##TYPE##This);                      \
        bool (*estVide)(Pile##TYPE##This);                     \
	} MetaPile##TYPE;                                          \
    void construire##TYPE(Pile##TYPE##This this);

#define IMPLEMENTER_META_PILE(TYPE)                            \
    MetaPile##TYPE LaMetaPile##TYPE = {                        \
        construire##TYPE,                                      \
        empiler##TYPE,                                         \
        depiler##TYPE,                                         \
        sommet##TYPE,                                          \
        estVide##TYPE                                          \
    };                                                         \
	void construire##TYPE(Pile##TYPE##This this) {             \
		this->top = this->pile;                                \
        this->class = &LaMetaPile##TYPE;                       \
	}                                                          
