#include "CLHEP/Random/MTwistEngine.h" // Entête pour MT dans la bibliothèque CLHEP
#include <fcntl.h>
#include <limits.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <iostream>
#include <pthread.h>

void* simulatePi(void *status){
	CLHEP::MTwistEngine *mtRng = new CLHEP::MTwistEngine();
	mtRng->restoreStatus((char *)status);

	long nbDraft = 1000000;
	long pointsInsideDisk = 0;

	for (long i = 0; i < nbDraft; i++) {
		double x = mtRng->flat();
		double y = mtRng->flat();
		if (x * x + y * y < 1) {
			pointsInsideDisk++;
		}
	}
	double pi = (double)pointsInsideDisk * 4 / nbDraft;

	delete mtRng;
	return new double(pi);
}


int main(int argc, char **argv) {
	pthread_t *tids = new pthread_t [argc - 1];
	double **trets = new double*[argc - 1];
	double totalPi = 0;

	for (size_t i = 1; i < argc; i++)
    	pthread_create(&tids[i-1],NULL, simulatePi, (void*)argv[i]);
	
	for (size_t i = 1; i < argc; i++){
		pthread_join(tids[i-1], (void **)&trets[i-1]);
		totalPi += *trets[i-1];
	}

	for (size_t i = 1; i < argc; i++)
		delete trets[i-1];
	delete [] trets;

	std::cout << "Pi average : " << totalPi / (argc - 1) << std::endl;

	return 0;
}
