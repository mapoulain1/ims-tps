#include "CLHEP/Random/MTwistEngine.h" // Entête pour MT dans la bibliothèque CLHEP
#include <fcntl.h>
#include <limits.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <sstream>


int main() {
	CLHEP::MTwistEngine *mtRng = new CLHEP::MTwistEngine();
	std::stringstream ss;
	long nbBatch = 10;
	long nbDraft = 2000000000;

	for (long i = 0; i < nbBatch; i++) {
		for (long j = 0; j < nbDraft; j++)
			mtRng->flat();
		
		ss.str("");
		ss << "MTStatus/MTStatus" << i;
		mtRng->saveStatus(ss.str().c_str());
	}

	delete mtRng;
	return 0;
}
