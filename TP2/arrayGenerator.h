#pragma once
#include <string.h>

void generateRandoms(double *data, long size);
void writeToFile(char *filename, double *data, long size);
void writeToFileBin(char *filename, double *data, long size);
void loadFileBin(char *filename, double *data, long *size);
