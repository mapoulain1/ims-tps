#pragma once
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>

/**
 * @brief Simule la valeur de PI avec Monte Carlo
 *
 * @param numberOfPoints Nombre de point à tirer
 * @return double Valeur de PI simulé
 */
double simulationPiMC(long numberOfPoints);

double genrand_real1_lookup(void);
double genrand_real1_lookup_bin(void);

long long timeval_diff(struct timeval *difference, struct timeval *end_time, struct timeval *start_time);