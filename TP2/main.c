#include "TP1.h"
#include "arrayGenerator.h"
#include "mt19937ar.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>

extern long tabMTSize;
extern double tabMT[];

long tabMTSizeBin;
double tabMTBin[];

void generateFile(long size) {
	double *data = malloc(size * sizeof(double));
	if (data == NULL) {
		fprintf(stderr, "Malloc error\n");
	}
	generateRandoms(data, size);
	writeToFile("generated.c", data, size);
	printf("Done writing %ld values.\n", size);
}

void generateFileBin(long size) {
	double *data = malloc(size * sizeof(double));
	if (data == NULL) {
		fprintf(stderr, "Malloc error\n");
	}
	generateRandoms(data, size);
	writeToFileBin("data.bin", data, size);
	printf("Done writing %ld values (binary).\n", size);
}

void testPiMC(void) {
	int rep = 10;
	double total = 0;
	double totalEcart = 0;
	struct timeval start_t, end_t;

	gettimeofday(&start_t, NULL);

	for (int i = 0; i < rep; i++) {
		double pi = simulationPiMC(1000000);
		total += pi;
		totalEcart += fabs(pi - M_PI);
	}
	gettimeofday(&end_t, NULL);
	printf("In %lld ms\n", timeval_diff(NULL, &end_t, &start_t) / 1000);

	printf("Average : %lf\n", total / rep);
	printf("Average deviation : %lf\n", totalEcart / rep);
}

int main(int argc, char const *argv[]) {
	(void)argc;
	(void)argv;
	init_genrand(548486529UL);

	// generateFile(25000000);
	// generateFileBin(2000);

	// loadFileBin("data.bin", tabMTBin, &tabMTSizeBin);

	testPiMC();

	return 0;
}
