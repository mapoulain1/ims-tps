#include "arrayGenerator.h"
#include "mt19937ar.h"
#include <stdio.h>
#include <stdlib.h>

void generateRandoms(double *data, long size) {
	for (long i = 0; i < size; i++)
		data[i] = genrand_real1();
}

void writeToFile(char *filename, double *data, long size) {
	FILE *file = fopen(filename, "w");
	if (file == NULL) {
		fprintf(stderr, "Can't open file %s", filename);
		return;
	}
	fprintf(file, "long tabMTSize = %ld;\n", size);
	fprintf(file, "double tabMT[] = {\n");
	for (long i = 0; i < size - 1; i++)
		fprintf(file, "%lf,", data[i]);
	fprintf(file, "%lf\n};\n", data[size - 1]);
	fclose(file);
}

void writeToFileBin(char *filename, double *data, long size) {
	FILE *file = fopen(filename, "w");
	if (file == NULL) {
		fprintf(stderr, "Can't open file %s", filename);
		return;
	}
	fwrite(&size, sizeof(long), 1, file);
	fwrite(data, sizeof(double), size, file);
	fclose(file);
}

void loadFileBin(char *filename, double *data, long *size) {
	FILE *file = fopen(filename, "w");
	if (file == NULL) {
		fprintf(stderr, "Can't open file %s", filename);
		return;
	}
	fread(size, sizeof(long), 1, file);
	fread(data, sizeof(double), *size, file);
	fclose(file);
}